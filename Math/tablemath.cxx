#include <iostream>
#include <vector>
#include <algorithm>    // std::sort
#include <ctime>
#include <../FHandler/fhandler.h>

time_t now = time(0);
tm* date = localtime(&now);

bool sortByDay(birthday_info &bday1, birthday_info &bday2) { 
	return (bday1.day < bday2.day);
};

bool sortByMonth(birthday_info &bday1, birthday_info &bday2) { 
	return (bday1.month < bday2.month);
};

bool sortByDayMonth(birthday_info &bday1, birthday_info &bday2) { 
	return (bday1.month < bday2.month) 
		or ((bday1.month == bday2.month) and (bday1.day < bday2.day));
};

void sort_Bdays(std::vector<birthday_info>* bdays){
	std::sort(bdays->begin(), bdays->end(), sortByDayMonth);
}

void sort_Bdays_time(std::vector<birthday_info>* bdays){
	std::sort(bdays->begin(), bdays->end(), sortByDayMonth);
	u_int cut = 0;
	std::vector<birthday_info> cbdays;
	for(auto it=bdays->begin(); it!=bdays->end(); it++){
		if ((it -> month >= date -> tm_mon+1) or
				(it->month == date -> tm_mon + 1 
				 and it -> day >= date -> tm_mday+1)){
			break;
		}
		cbdays.push_back(*it);
		cut += 1;
	}
	cbdays.insert(std::begin(cbdays), bdays->begin()+cut, bdays->end());
	*bdays = cbdays;
}
