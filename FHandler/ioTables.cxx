#include <iostream>
#include <fstream>
#include <vector>
#include <ctime>

bool write_birthday(std::string name, 
                    std::string date)
{
    tm tm1;
    int n = sscanf(date.c_str(), 
                "%2d.%2d,%4d", 
                &tm1.tm_mday, 
                &tm1.tm_mon, 
                &tm1.tm_year
                );
    if (n != 3)
        return false;

    std::ofstream myfile;
    myfile.open ("birthdays.txt", std::ios_base::app);
    myfile << name << "\t"
        << tm1.tm_mday << "\t"
        << tm1.tm_mon << "\t"
        << tm1.tm_year << std::endl;

    std::cout << "Saved " << name 
        << " birthday at " << tm1.tm_year
        << "-" << tm1.tm_mon << "-" << tm1.tm_mday
        << std::endl;
    return true;
}

struct birthday_info {
    std::string name; 
    int day; 
    int month; 
    int year;
};

std::vector<birthday_info> read_table(){
    std::vector<birthday_info> birthdays;
    std::string name; 
    int day, month, year;
   
    std::ifstream myfile("birthdays.txt");
    birthday_info b_info;
    while (myfile >> name >>  day >> month >> year){
        b_info.name = name;
        b_info.day = day;
        b_info.month = month;
        b_info.year = year;
        birthdays.push_back(b_info);
    }
    return birthdays;
};
