#include <iostream>
#include <fstream>

void print_birthday(
        std::string vorname, 
        std::string nachname, 
        int day, 
        int month, 
        int year){
    std::cout << vorname.c_str() << " "
        << nachname.c_str() << ": "
        << day << "."
        << month << " "
        << year << std::endl;
}

bool write_birthday(
        std::string vorname, 
        std::string nachname, 
        int day, 
        int month, 
        int year){
    std::ofstream myfile;
    myfile.open ("birthdays.txt");
    myfile << vorname.c_str() << "\t"
        << nachname.c_str() << "\t"
        << day << "\t"
        << month << "\t"
        << year << std::endl;
    return 0;
}

        
