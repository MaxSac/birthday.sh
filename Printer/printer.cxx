#include <string>
#include <iostream>
#include <vector>

struct birthday_info {
    std::string name; 
    int day; 
    int month; 
    int year;
};

void print_bday(birthday_info *bday){
    std::cout << bday->name << "\t"
        << ": " << bday->day
        << "." << bday->month
        << "," << bday->year
        << std::endl;
}

void print_bday(std::vector<birthday_info>* bdays){
    for(auto it = bdays -> begin(); it != bdays->end(); ++it)
        print_bday(&*it);
}

void print_help(){
    std::cout << "Usage: Birthday [options]\n"
        << "Display information about set birthday\n"
        << "Options: \n"
        << " -h \t \t this help with listed commands\n"
        << " --set \t \t set a birthday to memory\n"
        << " --show \t all entered birthdays sorted\n"
        << " --next \t next birthdays from nowday\n"
        << std::endl;
}
