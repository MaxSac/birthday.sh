#include <stdio.h>
#include <string>
#include <ctime>
#include <iostream>
#include "BirthdayConfig.h"
#include "FHandler/fhandler.h"
#include "Printer/printer.h"
#include "Math/tablemath.h"

#include <algorithm>

using namespace std;

int main(int argc, char *argv[])
{
    if (argc <2) {
        std::cout 
            << argv[0] << " Version-" 
            << Birthday_VERSION_MAJOR << "."
            << Birthday_VERSION_MINOR 
            << " -h for more infomation" << std::endl;
    }
    else if (argv[1] == string("--set") and argc == 4) {
        if (not write_birthday(argv[2], argv[3])){
            std::cout << argv[0] << " --set 'Name' '01.01,1990'" << std::endl;
        }
    }
    else if (argv[1] == string("--show")) {
        auto vec = read_table();
        sort_Bdays(&vec);
        print_bday(&vec);
    }
    else if (argv[1] == string("--next")) {
        auto vec = read_table();
		sort_Bdays_time(&vec);
        print_bday(&vec);
    }
    else if (argv[1] == string("-h")) {
        print_help();
    }
    else {
        birthday_info bday;
        bday.name = "Max Mustermann";
        bday.day=1;
        bday.month = 12;
        bday.year = 2000;

        print_bday(&bday);
    }
    return 0;
}
